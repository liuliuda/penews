$(document).ready(function () {
    var userName = GetCookie("username");
    $('#user_name').html(userName);
    $('#btn_logout').click(function () {
        delCookie("userId");
        delCookie("username");
        delCookie("token");
        window.location.href = "./login.html";
    });
});
