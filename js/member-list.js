$(document).ready(function () {
    getUserList();
});


function getUserList(param) {
    //登录的接口，通过拼接url来传入参数，分别传入用户名和密码
    var url = 'https://api2.bmob.cn/1/classes/_User';

    $.ajax({
        url: url,
        type: "get",
        async: true,
        data: param,
        headers: {
            "X-Bmob-Application-Id": "fe8e6e99eb8a3b8ef090f2458277a516 ",
            "X-Bmob-REST-API-Key": "0fc91c38bb894ec41b24434823a208e2 ",
            "Content-Type": "application/json",
        },
        // contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        success: function (data) {
            //如果data里面的code不为null的情况下，证明登录异常
            if (data.code != null) {

            } else {

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // 状态码
            console.log(XMLHttpRequest.status);
            // 状态
            console.log(XMLHttpRequest.readyState);
            // 错误信息
            console.log(XMLHttpRequest.responseText);

            layer.msg(JSON.stringify("请求失败"));
        }
    })
}